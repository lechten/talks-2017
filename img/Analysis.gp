set terminal pngcairo transparent enhanced fontscale 1.0 size 640,480
set output "Analysis.png"
set title "Exam vs JiTT (normalized results)"
set key off
set xtics nomirror out
set ytics nomirror out
set xrange [0:1.05]
set yrange [-0.05:1.05]
set xlabel "Exam result"
set ylabel "JiTT result" offset 1,0
set style fill solid 0.5 noborder
set linetype 1 linecolor rgb 'green'
set linetype 2 linecolor rgb 'red'
set arrow from 0.5,-0.05 to 0.5,1.05 nohead linecolor rgb 'red'
set arrow from 0,0.5 to 1.05,0.5 nohead linecolor rgb 'red'
plot 'Analysis.csv.agg' using 1:2:($3*0.0075):($1>=0.5?1:2) with circles linecolor variable
