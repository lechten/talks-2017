set terminal pngcairo transparent enhanced fontscale 1.0 size 640,480
set encoding utf8
set linetype 1 lc rgb 'red'
set style fill solid 0.5
set style histogram clustered gap 1
set xtics nomirror out
set ytics nomirror out
plot '$datafile' using 2:xticlabel(1) with histograms title "2016 (%)",\
     '$datafile' using 3:xticlabel(1) with histograms title "2017 (%)"
